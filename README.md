# Windows 10 crAPP Remover

	===========================================================================
     Created on:   	2018/11/09
     Author:        Timothy Gruber
     Website:       TimothyGruber.com
     GitLab:        https://gitlab.com/tjgruber/win10crappremover
	===========================================================================

![Win10crAPPRemover_AppAndPrivacySettings](https://gitlab.com/tjgruber/win10crappremover/wikis/uploads/b1d518808520b3fc67fc7ba1fef20413/Win10crAPPRemover_AppAndPrivacySettings.png)

# About
This PowerShell script is used to granularly remove unneeded or unwanted
applications and settings from Windows 10 easily via an intuitive GUI without
installing anything, with minimal requirements, and without the need to run the
script with switches or edit anything within the script. Everything is done via
the GUI.

    == Tested Against ==
        a. Windows 10 1803 (17134.1) Fresh
            a. Audit Mode Pre/post-updates
            b. User Mode Pre/post-updates
        b. Windows 10 1809 (17763.107) Fresh
            a. Audit Mode Pre/post-updates
            b. User Mode Pre/post-updates
